const logger = require('log4js').getLogger("PersonaController")
const express = require('express')
const PersonaDAO = require('../daos/PersonaDAO')
const router = express.Router()

router.get('/all', (req, res) => {
    logger.info("Entro al path /Persona/all")
    let personaDAO = new PersonaDAO();
    personaDAO.listar().then(datos => {
        res.json(datos);
    }).catch(error => {
        res.json({error: error})
    })
})

router.get('/all/count', (req, res) => {
    logger.info("Entro al path /Persona/all/count")
    let personaDAO = new PersonaDAO();
    personaDAO.total().then(datos => {
      res.json({total:datos});
    }).catch(error => {
        res.json({error: error})
    })
  })

router.get('/', (req, res) => {
    logger.info("Entro al path /Persona")
    let id = req.query.id;
    let personaDAO = new PersonaDAO()
    personaDAO.buscar(id).then(datos => {
      res.json(datos)
    }).catch(error => {
        res.json({error: error})
    })
})

router.post('/', (req, res) => {
    logger.info("Entro al path /Persona method POST")
    let personaDAO = new PersonaDAO()
    personaDAO.guardar(req.body).then(datos => {
        res.json(datos)
    }).catch(error => {
        res.json({error: error})
    })
})

router.put('/', (req, res) => {
    logger.info("Entro al path /Persona method PUT")
    let personaDAO = new PersonaDAO()
    personaDAO.actualizar(req.body).then(datos => {
        res.json(datos)
    }).catch(error => {
        res.json({error: error})
    })
})

router.delete('/', (req, res) => {
    logger.info("Entro al path /Persona method DELETE")
    let personaDAO = new PersonaDAO()
    personaDAO.eliminar(req.body._id).then(datos => {
        res.json(datos)
    }).catch(error => {
        res.json({error: error})
    })
})

module.exports = router
