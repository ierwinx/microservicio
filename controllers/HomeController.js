const logger = require('log4js').getLogger("HomeController")
const express = require('express')
const router = express.Router()

router.get('/', (req, res, next) => {
  logger.info("Entro al path /")

  let salida = {
    Endpoints: [
      {
        ruta: "/persona",
        method: "post",
        contentType: "application/json",
        descripcion: "Guarda un objeto persona"
      },
      {
        ruta: "/persona",
        method: "put",
        contentType: "application/json",
        descripcion: "Actualiza un objeto persona por _id"
      },
      {
        ruta: "/persona",
        method: "delete",
        contentType: "application/json",
        descripcion: "Elimina un objeto persona por _id"
      },
      {
        ruta: "/persona/all",
        method: "get",
        contentType: "application/json",
        descripcion: "Obtiene todos los objetos persona"
      },
      {
        ruta: "/persona/all/count",
        method: "get",
        contentType: "application/json",
        descripcion: "Cuenta todos los objetos persona"
      },
      {
        ruta: "/persona",
        method: "get",
        contentType: "application/json",
        descripcion: "Obtiene un objeto persona por query params id=5c6b44cd077f64451df2018d"
      }
    ]
  }

  res.json(salida)
})

module.exports = router
