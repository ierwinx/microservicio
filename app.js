require('./config/variables')
const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const log4js = require('./config/log4js');
const logger = require('log4js').getLogger("app")

const homeRouter = require('./controllers/HomeController')
const personaController = require('./controllers/PersonaController')

app.use(log4js);
app.use(express.json())
app.use(bodyParser.text())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/', homeRouter)
app.use('/persona', personaController)

app.listen(process.env.PORT, () => {
  logger.info("Inicia servicio en puerto " + process.env.PORT);
})

module.exports = app;
