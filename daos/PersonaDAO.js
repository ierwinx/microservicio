const logger = require('log4js').getLogger("PersonaDAO")
const Persona = require('../Models/Persona')

class PersonaDAO {

    constructor() {
    }

    async guardar(objeto) {
        logger.info(" ::: Guarda Informacion de una persona :::");
        var personas = new Persona(objeto);
        var errores = personas.validateSync();
        if (errores) {
            logger.error(" ::: Ocurrio un Error el las validaciones al guarda una persona :::");
            throw new Error(errores.message.replace('personas validation failed: ','ValidationError: '));
        } else {
            return await personas.save();
        }
    }

    async actualizar(objeto) {
        logger.info(" ::: Actualiza la Informacion de una persona por ID :::");
        var personas = new Persona(objeto);
        var errores = personas.validateSync();
        if (errores) {
            logger.error(" ::: Ocurrio un Error el las validaciones al actualizar una persona :::");
            throw new Error(errores.message.replace('personas validation failed: ','ValidationError: '));
        } else {
            return await Persona.findOneAndUpdate({ _id: objeto._id}, objeto).exec();
        }
    }

    async eliminar(id) {
        logger.info(" ::: Elimina una persona por ID :::");
        var respuesta = await Persona.deleteOne({ _id: id }).exec();
        return respuesta;
    }

    async listar() {
        logger.info(" ::: Consulta todos las personas :::");
        var respuesta = await Persona.find();
        return respuesta;
    }

    async buscar(id) {
        logger.info(" ::: Consulta una persona por ID :::");
        var respuesta = await Persona.findOne({ _id: id });
        return respuesta;
    }

    async total() {
        logger.info(" ::: Cuenta todos las personas :::");
        var respuesta = await Persona.countDocuments().exec();
        return respuesta;
    }

}

module.exports = PersonaDAO
